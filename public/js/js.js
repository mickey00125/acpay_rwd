$(function(){
    // 輪播
    $('.bxslider').bxSlider({
        mode: 'fade',
        captions: true,
        slideWidth: 960
    });

    // collapse
    $('.collapse-control').click(function(){
        var collapse_content = $(this).attr('data-collapse');

        $(this).toggleClass('collapse');
        $('#'+collapse_content).toggleClass('active');
    })

    // 訂購數量增減
    $('.quantity-button').off('click').on('click', function () {
        if ($(this).hasClass('quantity-add')) {
          var addValue = parseInt($(this).parent().find('input').val()) + 1;
              $(this).parent().find('input').val(addValue).trigger('change');
          }
      
          if ($(this).hasClass('quantity-remove')) {
          var removeValue = parseInt($(this).parent().find('input').val()) - 1;
              if( removeValue == 0 ) {
            removeValue = 1;
              }
              $(this).parent().find('input').val(removeValue).trigger('change');
          }
      });


    // input radio active
    $('.radio-label input').off('change').on('change', function() {
        $(this).parent().parent().parent().find('.radio-item').removeClass('active');
        $(this).parent().parent().addClass('active');
    });

    // 發票收據 input radio active
    $('.invoice-control input').off('change').on('change', function() {
        $(this).closest('#payuser').find('#invoice_content').toggleClass('active');
    });

    // 付款方式 input radio active
    $('.payment-control input').off('change').on('change', function() {
        $('.form-item-radio').removeClass('active');
        $(this).closest('.form-item-radio').toggleClass('active');
        if( $(this).attr('value') == 'creditcard' ){
            $('#credit_content').toggleClass('active');
        }
    });
  })
  